---
title: critique de la dialectique hégélienne
numero: 1
liens: ["critique de la raison kantienne", "féérie dialectique"]
---

(Dialectique à l'arrêt, dialectique nue, critique du Progrès, critique de la synthèse facile et de la positivité annihilatrice)

> Comment cet être-maintenant (qui n'est rien moins que l'être-maintenant du "temps présent" et qui est, au contraire, un être-maintenant intermittent, discontinu) peut-il, par lui-même, représenter déjà une concrétion plus haute ? Cette question, il est vrai, ne peut être abordée par la méthode dialectique dans le cadre d'une idéologie du progrès. (Les passages, trad. Lacoste)