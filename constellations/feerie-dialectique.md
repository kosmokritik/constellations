---
title: féérie dialectique 
numero: 4
liens: ["critique de la dialectique hégélienne", "magie prosaïque"]
---

(dialectique du rêve et de l'éveil, image dialectique) [cf Sur le surréalisme, Le livre des passages, Lettre à Greta ; JM Lachaud p.63-65]
