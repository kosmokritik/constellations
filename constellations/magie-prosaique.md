---
title: "magie prosaïque"
numero: 3
liens: ["féérie dialectique", "la mort du héros"]
---

(style objectif, la question de l'indicible, confrontation avec le Tractatus)

> Rêver de la fleur bleue, ce n’est plus de saison.

plusieurs elements à articuler : a) cette litteralité insensée des traductions de Sophocle par Hölderlin (cf La tâche du traducteur); b) ce style objectif, sobre, resserré sur son objet (cf Lettre à Greta) ; c) "exactitude automatique" ne laissant aucune place au "petit sou du sens" (cf Le surréalisme)
