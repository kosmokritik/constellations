---
Titre: ontologie messianique I (temps historique)
Numero: 11
Liens: ["critique de la dialectique hégélienne", "ontologie messianique II (espace de la vie)"]
---
(autre façon d'aborder la question du messianisme, d'introduire un concept de "communisme déjà existant" et de neutraliser le fétiche du "sugissement à venir" ; inviter à faire penser rêver le communisme hic et nunc ; ne plus chanter les fleurs tout en les piétinant)

(Temps remémoré vs temps vécu, celui de la fausse actualité, "l’imperfection incurable dans l’essence même du présent" cf L’image proustienne ; ce temps du souvenir est le terrain d’une "dialectique du bonheur" qui annonce déjà peut-être la féerie dialectique des Passages, voir également "rêve éveillé" ;  "L’éternité sur laquelle Proust ouvre quelques fenêtres repose sur l’entrecroisement des aspects du temps, non sur le temps illimité." : une citation qui me paraît cruciale pour désigner cette actualisation du temps historique en dehors du mauvais infini libéral-progressiste ; voir egalement p.150 cf Oeuvres II le concept de présentification, et se demander si le mouvement qui part d'une conception proustienne de l'actualité temporelle ne rencontre pas une conception renouvellée du temps historique dans les Thèses.)
