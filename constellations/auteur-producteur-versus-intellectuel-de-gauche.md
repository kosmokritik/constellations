---
Titre: auteur-producteur versus intellectuel de gauche
Numero: 8
Liens: ["technique & aura", "expérience du communisme"]
---
(Note sur Journal de Moscou : Durant son séjour à Moscou, en 1926, conscient déjà des formes de censure, objectives ou subjectives, qui menacent les arts soviétiques, Walter Benjamin se demande comment parvenir à un art qui soit  “à la fois expression et communication”. Dans “L’auteur comme producteur”, il invite pour se faire à ne plus considérer l’engagement d’un artiste depuis son adhésion idéologique à telle ou telle idée plus ou moins révolutionnaire, progressiste ou consensuelle, mais exclusivement depuis sa position de producteur. L’intérêt de l’artiste, en tant que producteur, le conduit à défendre l’intérêt du prolétariat sur le plan économique et à défendre sa liberté d’expérimentation sur le plan artistique.)

(Cf L'auteur comme producteur ; sa critique de l'intellectuel du Front populaire et de l'intellectuel bourgeois antifasciste )